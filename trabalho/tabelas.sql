/*
 filme tem (1)genero
 */

create table genero(
codG serial,
nome varchar(100),
constraint pkGenero primary key(codG)
);

create table filme(
nome varchar(100),
codG integer,
....
constraint fkGenero foreign key (codG) references genere(codG)
);
