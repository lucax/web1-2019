function calc() {

	var n1 = document.getElementById("camp1");
	var n2 = document.getElementById("camp2");
	var oper = document.getElementById("operador");
	var resultado = document.getElementById("resultado");
	
	if (n1.value == "" || n2.value == "") {
		console.log("preencha as campos");
		return(false);
	}

	var result;
	var x = parseInt(n1.value);
	var y = parseInt(n2.value);

	if(oper.value == "+") {
		result = x + y;
	}
	else if(oper.value == "-") {
		result = x - y;
	}
	else if(oper.value == "*") {
		result = x * y;
	}
	else if(oper.value == "/") {
		result = x / y;
	}

	resultado.innerHTML = result;
}
var b = document.getElementById("botao");
b.addEventListener("click", calc);
