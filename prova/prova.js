function paragrafo(contexto){
	return ($("<div id='box'><p id='resultado'></p></div>").html(contexto));
}

$(document).ready(function(){
	$("#botao").click(function(){

		var texto = $("#texto").val();
		var cor = $("#cor option:selected").val();
		var fonte = $("#fonte option:selected").val();
		var tamanho = $("#tamanho option:selected").val();

		$(this).after(paragrafo(texto));

		$("#box").css({"background-color": cor});
		$("p").css({"font-style": fonte, "font-size": tamanho});

	});
});
